<?php
declare(strict_types=1);

namespace Thessia\Models;

use Thessia\Database\MongoCollection;

class Users extends MongoCollection
{
    public string $collectionName = 'users';
    public string $indexField = 'email';
    public array $required = ['username', 'password', 'email'];
    public array $hiddenFields = ['password'];
}
