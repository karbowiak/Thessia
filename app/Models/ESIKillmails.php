<?php

declare(strict_types=1);

namespace Thessia\Models;

use Thessia\Database\MongoCollection;

class ESIKillmails extends MongoCollection
{
    /** @var string Name of collection in database */
    public string $collectionName = 'ESIKillmails';

    /** @var string Name of database that the collection is stored in */
    public string $databaseName = 'app';

    /** @var string Primary index key */
    public string $indexField = 'killID';

    /** @var string[] $hiddenFields Fields to hide from output (ie. Password hash, email etc.) */
    public array $hiddenFields = [];

    /** @var string[] $required Fields required to insert data to model (ie. email, password hash, etc.) */
    public array $required = ['killID', 'hash', 'fetched', 'dateFetched', 'fromDay', 'raw'];

    /**
     * @return array[]
     *
     * @psalm-return array{0: array, 1: array, 2: array, 3: array}
     */
    public function getIndex(): array
    {
        return [
            $this->createIndex()->addKey('killID')->isUnique()->end(),
            $this->createIndex()->addKey('fetched')->end(),
            $this->createIndex()->addKey('processed')->end(),
            $this->createIndex()->addKey('fromDay')->end()
        ];
    }
}
