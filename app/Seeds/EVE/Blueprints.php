<?php

namespace Thessia\Seeds\EVE;

use Thessia\Seed\AbstractEVESeed;

class Blueprints extends AbstractEVESeed
{
    protected string $sdeFileName = 'blueprints';
    protected string $collectionClass = \Thessia\Models\EVE\Blueprints::class;

    public function seed(): void
    {
        $collection = $this->getCollection();
        $collection->truncate();

        $data = $this->getYamlData();
        dd($data);
        foreach ($data as $elem) {
            $collection->setData($elem);
            $collection->save();
        }
    }
}
