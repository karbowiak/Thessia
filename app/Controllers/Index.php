<?php
declare(strict_types=1);

namespace Thessia\Controllers;

use Psr\Http\Message\ResponseInterface;
use Thessia\Attributes\UrlAttribute;

class Index extends ControllerAbstract
{
    #[UrlAttribute('/')]
    public function index(): ResponseInterface
    {
        return $this->render('index.twig');
    }
}
