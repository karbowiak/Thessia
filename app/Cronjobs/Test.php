<?php

namespace Thessia\Cronjobs;

use Thessia\Cron\CronAbstract;

class Test extends CronAbstract
{
    public string $cronTime = '* * * * *';

    public function handle(): void
    {
        $this->print('test..');
    }
}
