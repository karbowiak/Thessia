<?php

declare(strict_types=1);

namespace Thessia\Commands\EVE;

use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use Thessia\Console\ConsoleCommandAbstract;

class Seeds extends ConsoleCommandAbstract
{
    public string $signature = 'eve:seeds';
    public string $description = 'Populate the EVE SDE Tables';

    public function __construct(
        protected Container $container
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $seeds = new ComposerFinder();
        $seeds->inNamespace('Thessia\\Seeds\\EVE');

        // @TODO This doesn't find any seeds for reasons that are incomprehensible
        // Use the classLoader instead..
        foreach ($seeds as $className => $reflection) {
            dump('hi');
        }
    }
}
