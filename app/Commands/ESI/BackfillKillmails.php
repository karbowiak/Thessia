<?php

declare(strict_types=1);

namespace Thessia\Commands\ESI;

use IteratorIterator;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Models\ESIKillmails;
use Thessia\Queues\ESIKillmailFetch;

class BackfillKillmails extends ConsoleCommandAbstract
{
    public string $signature = 'esi:backfill:killmails';
    public string $description = 'Backfills killmail data from ESI';


    public function __construct(
        protected ESIKillmails $ESIKillmails,
        protected ESIKillmailFetch $ESIKillmailFetch
    ) {
        parent::__construct();
    }


    final public function handle(): void
    {
        $killmails = $this->ESIKillmails->collection->find(['fetched' => false]);
        $iterator = new IteratorIterator($killmails);
        $iterator->rewind();

        while ($killmail = $iterator->current()) {
            $this->ESIKillmailFetch->enqueue((array) $killmail);
            $iterator->next();
        }
    }
}
