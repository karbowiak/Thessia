<?php

declare(strict_types=1);

namespace Thessia\Commands;

use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Queue\Queue as QueueInstance;

class Queue extends ConsoleCommandAbstract
{
    public string $signature = 'queue';
    public string $description = 'Runs the Queue (Based on Hermes)';

    public function __construct(
        protected QueueInstance $queue,
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $dispatcher = $this->queue->getDispatcher();
        $dispatcher->handle();
    }
}
