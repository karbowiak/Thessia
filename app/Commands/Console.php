<?php

namespace Thessia\Commands;

use Thessia\Redis\RedisCache;
use League\Container\Container;
use Thessia\Redis\RedisConnection;
use Thessia\Database\MongoConnection;
use Thessia\Database\MongoCollection;
use Thessia\Console\ConsoleCommandAbstract;
use Psy\Configuration;
use Psy\Shell;
use Thessia\Logger\Logger;

/**
 * @property $manualPath
 */
class Console extends ConsoleCommandAbstract
{
    protected string $signature = 'console { --manualPath=/usr/local/share/psysh : The path to store the PHP Manual }';

    protected string $description = 'Console';

    public function __construct(
        protected Container $container,
        protected RedisConnection $redisConnection,
        protected RedisCache $redisCache,
        protected MongoConnection $mongoConnection,
        protected MongoCollection $mongoCollection,
        protected Logger $logger
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        try {
            // Check for php_manual
            $manualPath = $this->manualPath;
            $manualName = 'php_manual.sqlite';
            if (!is_dir($manualPath) && !mkdir($manualPath, 0777, true) && !is_dir($manualPath)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $manualPath));
            }
            if (!file_exists($manualPath . '/' . $manualName)) {
                $this->out('Downloading PHP Manual, one moment...');
                copy('http://psysh.org/manual/en/php_manual.sqlite', $manualPath . '/' . $manualName);
            }
        } catch (\Exception $e) {
            $this->out("<bg=red>{$e->getMessage()}</>");
        }
        $shell = new Shell(new Configuration([]));
        $shell->setScopeVariables([
            'container' => $this->container,
            'redisConnection' => $this->redisConnection,
            'redisCache' => $this->redisCache,
            'mongoConnection' => $this->mongoConnection,
            'mongoCollection' => $this->mongoCollection,
            'logger' => $this->logger,
        ]);

        $shell->run();
    }
}
