<?php

declare(strict_types=1);

namespace Thessia\Commands\Info;

use Composer\Autoload\ClassLoader;
use Kcs\ClassFinder\Finder\ComposerFinder;
use ReflectionClass;
use RuntimeException;
use Thessia\Attributes\UrlAttribute;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Controllers\API\ControllerAbstract;

class Routes extends ConsoleCommandAbstract
{
    public string $signature = 'info:routes { --api : Only list API endpoints }';
    public string $description = 'List all the routes available for the server';

    public function __construct(
        protected ClassLoader $autoloader
    ) {
        parent::__construct();
    }


    final public function handle(): void
    {
        $endpoints = [];

        // Controller Routes
        $controllers = new ComposerFinder();
        $controllers->inNamespace('Thessia\\Controllers');
        /** @var ReflectionClass $reflection */
        foreach ($controllers as $className => $reflection) {
            try {
                foreach ($reflection->getMethods() as $method) {
                    $attributes = $method->getAttributes(UrlAttribute::class);
                    foreach ($attributes as $attribute) {
                        $apiUrl = $attribute->newInstance();
                        $endpoints[] = [
                            $apiUrl->getRoute(),
                            implode(',', $apiUrl->getType()),
                            $className,
                            $method->getName()
                        ];
                    }
                }
            } catch (\Throwable $e) {
                throw new RuntimeException('Error loading controller: ' . $e->getMessage());
            }
        }

        $this->table(['URL', 'Request Type', 'Class', 'Method'], $endpoints);
    }
}
