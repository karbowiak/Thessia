<?php

declare(strict_types=1);

namespace Thessia\Commands\zKB;

use GuzzleHttp\Client;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Queues\zKillboardHistory;

class zKBHistory extends ConsoleCommandAbstract
{
    public string $signature = 'zkb:history ';
    public string $description = 'Fetches the killmail history from zKillboard';

    public function __construct(
        protected zKillboardHistory $zKBHistoryQueue
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $guzzle = new Client();
        $result = $guzzle->get('https://zkillboard.com/api/history/totals.json');
        if ($result->getStatusCode() === 200) {
            $totals = $this->jsonDecode($result->getBody()->getContents());
            foreach ($totals as $date => $count) {
                $this->zKBHistoryQueue->enqueue(['date' => $date, 'count' => $count]);
            }
        }
    }

    private function jsonDecode(string $json): array
    {
        return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    }
}
