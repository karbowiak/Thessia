<?php

declare(strict_types=1);

namespace Thessia\Commands;

use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use Thessia\Console\ConsoleCommandAbstract;

class MongoIndexes extends ConsoleCommandAbstract
{
    public string $signature = 'mongo:indexes';
    public string $description = 'Ensures indexes for mongodb collections';

    public function __construct(
        protected Container $container
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $models = new ComposerFinder();
        $models->inNamespace('Thessia\\Models');

        foreach ($models as $className => $reflection) {
            try {
                /** @var \Thessia\Database\MongoCollection $model */
                $model = $this->container->get($className);
                $index = $model->getIndex();
                $model->collection->createIndexes($index);
            } catch (\Exception $e) {
                $this->logger->error('Error occurred when generating indexes: ' . $e->getMessage(), $e->getTrace());
                $this->out('Error occurred when generating indexes: ' . $e->getMessage());
            }
        }
    }
}
