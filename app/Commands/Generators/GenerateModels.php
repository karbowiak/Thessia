<?php

namespace Thessia\Commands\Generators;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;
use RuntimeException;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Database\MongoCollection;
use Thessia\Generator\GeneratorManager;
use Thessia\Templates\Generators\ModelTemplate;

class GenerateModels extends ConsoleCommandAbstract
{
    protected string $signature = 'generate:model';

    protected string $description = 'Generate Controller';

    public function __construct(
        protected GeneratorManager $generatorManager
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $this->out('You are now trying to generate a <info>Model</info>. - Please refer to the Generator Docs for info on how to use this.');

        $collectionName = $this->ask('<info>Name:</info> (Tip, use / to subdivide into folders, eg: eve/universe (Only 1 deep)');
        $explodedName = explode('/', $collectionName);

        $preClassName = count($explodedName) > 1 ? ucfirst($explodedName[0]) : null;
        $className = count($explodedName) > 1 ? ucfirst($explodedName[1]) : $collectionName;
        $namespace = count($explodedName) > 1 ?
            "Thessia\\Models\\{$preClassName}" :
            "Thessia\\Models";

        $file = new PhpFile();
        $file->setStrictTypes(true);

        $namespace = $file->addNamespace($namespace);
        $namespace->addUse(MongoCollection::class);

        $class = ClassType::from(ModelTemplate::class);
        $class->setName(count($explodedName) > 1 ? $preClassName . $className : $className);
        $class->addProperty('collectionName')->setType('string')->setValue(lcfirst($preClassName . $className))->addComment('@var string Name of collection in database');

        $namespace->add($class);

        $printer = new Printer();
        $generatedCode = $printer->printFile($file);

        $folderPath = count($explodedName) > 1 ?
            dirname(__DIR__, 2) . "/Models/{$preClassName}" :
            dirname(__DIR__, 2) . "/Models";

        if (@!mkdir($folderPath, 0777, true) && !is_dir($folderPath)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $folderPath));
        }

        $filePath = count($explodedName) > 1 ?
            $folderPath . "/{$preClassName}{$className}.php":
            $folderPath . "/{$className}.php";

        if ($this->overwrite && is_file($filePath)) {
            throw new RuntimeException('Error, file already exists..');
        }

        try {
            file_put_contents($filePath, $generatedCode, LOCK_EX);
            $this->out("{$className} generated and placed in: {$filePath}");
        } catch (Exception $e) {
            $this->out("An error occurred: {$e->getMessage()}");
        }
    }
}
