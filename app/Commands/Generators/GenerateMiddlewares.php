<?php

namespace Thessia\Commands\Generators;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Generator\GeneratorManager;
use Thessia\Templates\Generators\MiddlewareTemplate;

class GenerateMiddlewares extends ConsoleCommandAbstract
{
    protected string $signature = 'generate:middleware';

    protected string $description = 'Generate Controller';

    public function __construct(
        protected GeneratorManager $generatorManager
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $this->out('You are now trying to generate a <info>Middleware</info>. - Please refer to the Generator Docs for info on how to use this.');

        $middlewareName = $this->ask('<info>Name:</info>');
        $namespace = "Thessia\\Middlewares";

        $file = new PhpFile();
        $file->setStrictTypes(true);

        $namespace = $file->addNamespace($namespace);
        $namespace->addUse(ServerRequestInterface::class);
        $namespace->addUse(RequestHandlerInterface::class);
        $namespace->addUse(ResponseInterface::class);
        $namespace->addUse(Psr17Factory::class);
        $namespace->addUse(MiddlewareInterface::class);

        $class = ClassType::from(MiddlewareTemplate::class);
        $class->setName($middlewareName);
        $constructor = $class->addMethod('__construct');
        $constructor
            ->addPromotedParameter('responseFactory')
            ->setType(Psr17Factory::class)
            ->setPrivate();

        $process = $class->addMethod('process');
        $process
            ->addParameter('request')
            ->setType(ServerRequestInterface::class);
        $process
            ->addParameter('next')
            ->setType(RequestHandlerInterface::class);
        $process->addBody('
try {
    return $next->handle($request);
} catch (\Throwable $e) {
    $response = $this->responseFactory->createResponse(500);
    $response->getBody()->write(json_encode([\'Error occurred\' => $e->getMessage()]));
    $response->withHeader(\'Content-Type\', \'application/json\');
    return $response;
}');
        $process->setReturnType(ResponseInterface::class);

        $namespace->add($class);
        $printer = new Printer();
        $generatedCode = $printer->printFile($file);

        $folderPath = dirname(__DIR__, 2) . "/Middlewares";

        if (@!mkdir($folderPath, 0777, true) && !is_dir($folderPath)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $folderPath));
        }

        $filePath = $folderPath . "/{$middlewareName}.php";

        try {
            file_put_contents($filePath, $generatedCode, LOCK_EX);
            $this->out("{$middlewareName} generated and placed in: {$filePath}");
        } catch (Exception $e) {
            $this->out("An error occurred: {$e->getMessage()}");
        }
    }
}
