<?php

namespace Thessia\Commands\Generators;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;
use RuntimeException;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Generator\GeneratorManager;
use Thessia\Templates\Generators\CommandTemplate;

/**
 * @property bool $overwrite
 */
class GenerateCommands extends ConsoleCommandAbstract
{
    protected string $signature = 'generate:command { --overwrite : Overwrite in case the file already exists }';

    protected string $description = 'Generate CLi command';

    public function __construct(
        protected GeneratorManager $generatorManager
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $this->out('You are now trying to generate a <info>Command</info>. - Please refer to the Generator Docs for info on how to use this.');
        $name = $this->ask('<info>Name:</info>');
        $sig = strtolower($name) . ' ' . $this->ask('<info>Signature:</info>');
        $descr = $this->ask('<info>Description:</info>');

        $explodedName = explode(':', $name);
        $folderName = count($explodedName) > 1 ? ucfirst($explodedName[0]) : null;
        $className = count($explodedName) > 1 ? ucfirst($explodedName[1]) : ucfirst($name);
        $namespaceName = count($explodedName) > 1 ? "Thessia\\Commands\\{$folderName}" : "Thessia\\Commands";

        $file = new PhpFile();
        $file->setStrictTypes(true);
        $namespace = $file->addNamespace($namespaceName);
        $namespace->addUse(ConsoleCommandAbstract::class);
        $class = ClassType::from(CommandTemplate::class);
        $class->setName(count($explodedName) > 1 ? $folderName . $className : $className);
        $class->addProperty('signature')->setType('string')->setValue($sig);
        $class->addProperty('description')->setType('string')->setValue($descr);
        $namespace->add($class);

        $printer = new Printer();
        $generatedCode = $printer->printFile($file);

        $folderPath = count($explodedName) > 1 ?
            dirname(__DIR__, 2) . "/Commands/{$folderName}" :
            dirname(__DIR__, 2) . "/Commands";

        if (@!mkdir($folderPath, 0777, true) && !is_dir($folderPath)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $folderPath));
        }

        $filePath = count($explodedName) > 1 ?
            $folderPath . "/{$folderName}{$className}.php":
            $folderPath . "/{$folderName}.php";

        if ($this->overwrite && is_file($filePath)) {
            throw new RuntimeException('Error, file already exists..');
        }

        try {
            file_put_contents($filePath, $generatedCode, LOCK_EX);
            $this->out("{$className} generated and placed in: {$filePath}");
        } catch (Exception $e) {
            $this->out("An error occurred: {$e->getMessage()}");
        }
    }
}
