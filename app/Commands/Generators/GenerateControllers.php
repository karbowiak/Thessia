<?php

namespace Thessia\Commands\Generators;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;
use RuntimeException;
use Thessia\Console\ConsoleCommandAbstract;
use Thessia\Generator\GeneratorManager;
use Thessia\Templates\Generators\ControllerTemplate;

class GenerateControllers extends ConsoleCommandAbstract
{
    protected string $signature = 'generate:controller';

    protected string $description = 'Generate Controller';

    public function __construct(
        protected GeneratorManager $generatorManager
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $this->out('You are now trying to generate a <info>Controller</info>. - Please refer to the Generator Docs for info on how to use this.');

        $controllerName = $this->ask('<info>Name:</info>');
        $namespace = "Thessia\\Controllers";

        $file = new PhpFile();
        $file->setStrictTypes(true);

        $namespace = $file->addNamespace($namespace);

        $class = ClassType::from(ControllerTemplate::class);
        $class->setName($controllerName);
        $namespace->add($class);
        $printer = new Printer();
        $generatedCode = $printer->printFile($file);

        $folderPath = dirname(__DIR__, 2) . "/Controllers";

        if (@!mkdir($folderPath, 0777, true) && !is_dir($folderPath)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $folderPath));
        }

        $filePath = $folderPath . "/{$controllerName}.php";

        try {
            file_put_contents($filePath, $generatedCode, LOCK_EX);
            $this->out("{$controllerName} generated and placed in: {$filePath}");
        } catch (Exception $e) {
            $this->out("An error occurred: {$e->getMessage()}");
        }
    }
}
