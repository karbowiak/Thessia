<?php

namespace Thessia\Queues;

use GuzzleHttp\Client;
use Thessia\Models\ESIKillmails;
use Thessia\Queue\AbstractQueue;
use Thessia\Redis\RedisConnection;
use Tomaj\Hermes\MessageInterface;

class zKillboardHistory extends AbstractQueue
{
    protected int $maxRetry = 10;

    public function __construct(
        RedisConnection $connection,
        protected \Thessia\Logger\Logger $logger,
        protected \Thessia\Models\ESIKillmails $ESIKillmails
    ) {
        parent::__construct($connection, $logger);
    }

    /**
     * @return true
     */
    public function handle(MessageInterface $message): bool
    {
        $payload = $message->getPayload();
        $date = $payload['date'];

        $guzzle = new Client();
        $request = $guzzle->get('https://zkillboard.com/api/history/' . $date . '.json');

        if ($request->getStatusCode() === 200) {
            $killmails = $this->jsonDecode($request->getBody()->getContents());
            foreach ($killmails as $killID => $hash) {
                $this->ESIKillmails->collection->insertOne([
                    'killID' => $killID,
                    'hash' => $hash,
                    'fetched' => false,
                    'dateFetched' => null,
                    'fromDay' => $date
                ]);
            }
        }

        return true;
    }
}
