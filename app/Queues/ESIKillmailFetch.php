<?php

namespace Thessia\Queues;

use GuzzleHttp\Client;
use Thessia\Logger\Logger;
use Thessia\Models\ESIKillmails;
use Thessia\Queue\AbstractQueue;
use Thessia\Redis\RedisConnection;
use Tomaj\Hermes\MessageInterface;

class ESIKillmailFetch extends AbstractQueue
{
    protected int $maxRetry = 10;

    public function __construct(
        RedisConnection $redisConnection,
        protected Logger $logger,
        protected ESIKillmails $ESIKillmails
    ) {
        parent::__construct($redisConnection, $logger);
    }

    /**
     * @return true
     */
    public function handle(MessageInterface $message): bool
    {
        $payload = $message->getPayload();
        $killID = $payload['killID'];
        $hash = $payload['hash'];

        $guzzle = new Client();
        $request = $guzzle->get("https://esi.evetech.net/latest/killmails/{$killID}/{$hash}");

        if ($request->getStatusCode() === 200) {
            $killmail = $this->jsonDecode($request->getBody()->getContents());
            $this->ESIKillmails->update(['killID' => $killID], ['$set' => [
                'fetched' => true,
                'raw' => $killmail,
                'dateFetched' => $this->ESIKillmails->makeTimeFromUnixTime(time()),
                'processed' => false
            ]]);
        }

        return true;
    }
}
