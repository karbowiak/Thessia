<?php
declare(strict_types=1);

namespace Thessia\Templates\Generators;

use Thessia\Console\ConsoleCommandAbstract;

class CommandTemplate extends ConsoleCommandAbstract
{
    protected string $signature = '';

    protected string $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    final public function handle(): void
    {
    }
}
