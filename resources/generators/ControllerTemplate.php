<?php
declare(strict_types=1);

namespace Thessia\Templates\Generators;

use Psr\Http\Message\ResponseInterface;
use Thessia\Attributes\UrlAttribute;
use Thessia\Controllers\ControllerAbstract;

class ControllerTemplate extends ControllerAbstract
{
    #[UrlAttribute('/exampleEndpoint')]
    public function example(): ResponseInterface
    {
        return $this->json(['test' => 'test']);
    }
}
