<?php

$mongoUsername = 'thessia';
$mongoPassword = 'eb2dd30f0dd8e920d49e027fc0440505';

return [
    'servers' => isset($_SERVER['MONGO_HOST']) ? explode(',', $_SERVER['MONGO_HOST']) : [
        '127.0.0.1:27017'
    ],
    'options' => [
        'username' => 'root', //$_SERVER['MONGO_USERNAME'] ?? $mongoUsername,
        'password' => $_SERVER['MONGO_PASSWORD'] ?? $mongoPassword,
        'connectTimeoutMS' => $_SERVER['MONGO_CONNECTTIMEOUT'] ?? 50000,
        'socketTimeoutMS' => $_SERVER['MONGO_SOCKETTIMEOUT'] ?? 50000,
        'authSource' => $_SERVER['MONGO_AUTHSOURCE'] ?? 'admin',
        'replicaSet' => $_SERVER['MONGO_REPLICASET'] ?? 'rs0'
    ],
    'typeMap' => [
        'root' => 'object',
        'document' => 'object',
        'array' => 'object'
    ],
    'db' => $_SERVER['MONGO_DATABASE'] ?? 'thessia'
];
