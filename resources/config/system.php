<?php

return [
    'papertrail' => [
        'host' => 'logs2.papertrailapp.com',
        'port' => 26034
    ]
];
