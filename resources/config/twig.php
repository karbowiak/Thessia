<?php

return[
    'template_path' => isset($_SERVER['TWIG_TEMPLATE_DIR']) ? $_SERVER['TWIG_TEMPLATE_DIR'] : dirname(__DIR__, 1) . '/templates/',
    'cache_path' => isset($_SERVER['TWIG_CACHE_DIR']) ? $_SERVER['TWIG_CACHE_DIR'] : dirname(__DIR__, 1) . '/cache/twig',
    'debug' => isset($_SERVER['TWIG_DEBUG']) ? $_SERVER['TWIG_DEBUG'] === '1' : true,
    'auto_reload' => isset($_SERVER['TWIG_AUTO_RELOAD']) ? $_SERVER['TWIG_AUTO_RELOAD'] === '1' : true,
    'strict_variables' => isset($_SERVER['TWIG_STRICT_VARIABLES']) ? $_SERVER['TWIG_STRICT_VARIABLES'] === '1' : true,
    'optimizations' => isset($_SERVER['TWIG_OPTIMIZATIONS']) ? $_SERVER['TWIG_OPTIMIZATIONS'] === '1' : true
];
