# Generate:command

Commands can contain parameters, options and arguments.

Example:
```php
    protected string $signature = 'commandType:command
    { parOne : This param is required }
    { parTwo? : This param is optional }
    { --argument=1 : This argument defaults to 1 }
    { --argument : This argument is true or false }';
```

# Creating command

When creating the command, you will be asked for defining the Name, Signature and Description

Example:
```shell
➜ php bin/thessia generate:command
You are now trying to generate a Command. - Please refer to the Generator Docs for info on how to use this.
Name: test:test
Signature: { name? : What is my name? } { --uppercase }
Description: This outputs the name, and if no name is input it outputs hello world
```

After a `composer update` you can now use this with `php bin/thessia test someName --uppercase`

This would generate the following file:
```php
namespace Thessia\Commands\Test;

use Thessia\Console\ConsoleCommandAbstract;

class Test extends ConsoleCommandAbstract
{
        public string $signature = 'test:test { name? : What is my name? } { --uppercase }';
        public string $description = 'This outputs the name, and if no name is input it outputs Hello World';


        public function __construct(string $name = 'null')
        {
        }


        final public function handle(): void
        {
        }
}
```