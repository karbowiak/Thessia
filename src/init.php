<?php

use Thessia\Bootstrap;

# Init composer
$autoloaderPath = __DIR__ . '/../vendor/autoload.php';
if (!file_exists($autoloaderPath)) {
    throw new RuntimeException('Error, composer is not setup correctly.. Please run composer install');
}
$autoloader = require __DIR__ . '/../vendor/autoload.php';

# Bootstrap the system
return [new Bootstrap($autoloader), $autoloader];
