<?php

namespace Thessia;

use Thessia\Config\MongoConfig;
use Thessia\Config\RedisConfig;
use League\Container\Container;
use Composer\Autoload\ClassLoader;
use League\Container\ReflectionContainer;
use Thessia\Config\SystemConfig;
use Thessia\Config\TwigConfig;

class Bootstrap
{
    public function __construct(
        protected ClassLoader $autoloader,
        public ?Container $container = null,
        protected ?MongoConfig $mongoConfig = null,
        protected ?RedisConfig $redisConfig = null,
        protected ?TwigConfig $twigConfig = null,
        protected ?SystemConfig $systemConfig = null
    ) {
        // Instantiate the configuration
        $this->mongoConfig = $this->mongoConfig ?? new MongoConfig();
        $this->redisConfig = $this->redisConfig ?? new RedisConfig();
        $this->twigConfig = $this->twigConfig ?? new TwigConfig();
        $this->systemConfig = $this->systemConfig ?? new SystemConfig();

        // Build the container
        $this->buildContainer($this->autoloader);
    }

    protected function buildContainer(ClassLoader $autoloader): void
    {
        // Instantiate a new container if the one we're given is null
        $this->container = $this->container ?? new Container();

        // Add configs
        $this->container->add(MongoConfig::class, $this->mongoConfig);
        $this->container->add(RedisConfig::class, $this->redisConfig);
        $this->container->add(TwigConfig::class, $this->twigConfig);
        $this->container->add(SystemConfig::class, $this->systemConfig);

        // Register ReflectionContainer, so AutoWiring is enabled
        $this->container->delegate(
            new ReflectionContainer()
        );

        // Add the autoloader
        $this->container->add(ClassLoader::class, $autoloader);

        // Add the container (Keep at the bottom)
        $this->container->add(Container::class, $this->container);
    }
}
