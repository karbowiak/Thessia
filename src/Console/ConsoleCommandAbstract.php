<?php

namespace Thessia\Console;

abstract class ConsoleCommandAbstract extends ConsoleCommandHelper
{
    protected string $signature;

    protected string $description;

    abstract public function handle(): void;
}
