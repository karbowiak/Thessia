<?php

namespace Thessia\Console;

use Exception;
use RuntimeException;
use League\Container\Container;
use Kcs\ClassFinder\Finder\ComposerFinder;
use Symfony\Component\Console\Application;

class Console
{
    public function __construct(
        public Application $console,
        public Container $container
    ) {
        $finder = new ComposerFinder();
        $finder->inNamespace('Thessia\\Commands');

        foreach ($finder as $className => $reflector) {
            $this->console->add($this->container->get($className));
        }
    }

    final public function run(): int
    {
        try {
            return $this->console->run();
        } catch (Exception $e) {
            ($this->container->get(\Thessia\Logger\Logger::class))
                ->debug('Error running CLi: ' . $e->getMessage(), $e->getTrace());
            throw new RuntimeException('Error running CLi: ' . $e->getMessage());
        }
    }
}
