<?php

namespace Thessia\Redis;

use Predis\Client;
use Thessia\Config\RedisConfig;

class RedisConnection
{
    private Client $redis;

    public function __construct(RedisConfig $redisConfig)
    {
        $redis = new Client($redisConfig->host(), [
            'replication' => 'sentinel',
            'service' => 'thessia',
            'autodiscovery' => true,
            'cluster' => 'predis',
            'parameters' => $redisConfig->parameters()
        ]);

        $this->redis = $redis;
    }

    final public function getRedis(): Client
    {
        return $this->redis;
    }
}
