<?php

namespace Thessia\Redis;

use Predis\Client;
use Exception;
use Illuminate\Support\Collection;

class RedisCache implements RedisCacheInterface
{
    protected Client $redis;

    public function __construct(RedisConnection $redisConnection)
    {
        $this->redis = $redisConnection->getRedis();
    }

    final public function get(string $key, null|bool|array|string $default = null): null|bool|mixed|array|string
    {
        try {
            return $this->has($key) ?
                json_decode($this->redis->get($key), true, 512, JSON_THROW_ON_ERROR) :
                $default;
        } catch (Exception $e) {
            return null;
        }
    }

    final public function set(string $key, null|bool|array|string $data, int $ttl = 0): bool
    {
        try {
            if ($ttl > 0) {
                return $this->redis->set($key, json_encode($data, JSON_THROW_ON_ERROR), $ttl);
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    final public function delete(string $key): bool
    {
        return $this->redis->del($key);
    }

    final public function getMultiple(array $keys, null|bool|array|string $default = null): Collection
    {
        $return = $this->redis->mget($keys);

        if (empty($return)) {
            return $default instanceof Collection ? $default : collect($default);
        }
        return $return;
    }

    final public function setMultiple(array $data, null|bool|array|string $default = null, int $ttl = 0): bool
    {
        if ($ttl > 0) {
            $multi = $this->redis->multi();
            foreach ($data as $key => $value) {
                $multi->set($key, $value, $ttl);
            }
            $multi->exec();

            return true;
        }

        return false;
    }

    /**
     * @return true
     */
    final public function deleteMultiple(array $keys): bool
    {
        $multi = $this->redis->multi();
        foreach ($keys as $key) {
            $multi->del($key);
        }
        $multi->exec();

        return true;
    }

    final public function clear(): bool
    {
        return $this->redis->flushDB();
    }

    final public function has(string $key): bool
    {
        return $this->redis->exists($key);
    }
}
