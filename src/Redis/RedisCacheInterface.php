<?php

namespace Thessia\Redis;

use Illuminate\Support\Collection;

interface RedisCacheInterface
{
    public function get(string $key, null|bool|array|string $default = null): null|bool|mixed|array|string;
    public function set(string $key, null|bool|array|string $data, int $ttl = 0): bool;
    public function delete(string $key): bool;
    public function getMultiple(array $keys, null|bool|array|string $default = null): Collection;
    public function setMultiple(array $data, null|bool|array|string $default = null): bool;
    public function deleteMultiple(array $keys): bool;
    public function clear(): bool;
    public function has(string $key): bool;
}
