<?php

namespace Thessia\Queue;

use Thessia\Redis\RedisConnection;
use Tomaj\Hermes\Dispatcher;
use Tomaj\Hermes\Emitter;
use Tomaj\Hermes\Handler\HandlerInterface;

abstract class AbstractQueue implements HandlerInterface
{
    protected Emitter $emitter;
    protected int $maxRetry = 1;

    public function __construct(
        RedisConnection $connection,
        protected \Thessia\Logger\Logger $logger
    ) {
        $driver = new \Tomaj\Hermes\Driver\PredisSetDriver($connection->getRedis());
        $this->emitter = new Emitter($driver);
    }

    public function enqueue(array $payload = [], string $priority = 'low'): void
    {
        $validPriorities = ['low' => -10, 'medium' => 0, 'high' => 10, 'immediate' => 50, 'cron' => 100];
        $priorityAmount = $validPriorities[$priority] ?? -10;

        $message = new \Tomaj\Hermes\Message($this::class, $payload, Dispatcher::DEFAULT_PRIORITY + $priorityAmount);
        $this->emitter->emit($message);
    }

    public function maxRetry(): int
    {
        return $this->maxRetry;
    }

    public function jsonDecode(string $json): array
    {
        return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    }
}
