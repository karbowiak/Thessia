<?php

namespace Thessia\Queue;

use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use Thessia\Logger\Logger;
use Thessia\Redis\RedisConnection;
use Tomaj\Hermes\Dispatcher;

class Queue
{
    protected Dispatcher $dispatcher;

    public function __construct(
        protected Container $container,
        protected RedisConnection $redisConnection,
        protected Logger $logger
    ) {
    }

    private function instantiate(): Dispatcher
    {
        $driver = new \Tomaj\Hermes\Driver\PredisSetDriver($this->redisConnection->getRedis());
        $driver->setupPriorityQueue('low', Dispatcher::DEFAULT_PRIORITY - 10);
        $driver->setupPriorityQueue('high', Dispatcher::DEFAULT_PRIORITY + 10);
        $driver->setupPriorityQueue('immediate', Dispatcher::DEFAULT_PRIORITY + 50);
        $driver->setupPriorityQueue('cron', Dispatcher::DEFAULT_PRIORITY + 100);
        
        $dispatcher = new Dispatcher($driver, $this->logger);

        $queues = new ComposerFinder();
        $queues->inNamespace('Thessia\\Queues');
        foreach ($queues as $className => $reflection) {
            /** @var AbstractQueue $instance */
            $instance = $this->container->get($className);
            $dispatcher->registerHandler($className, $instance);
        }

        return $dispatcher;
    }

    public function getDispatcher(): Dispatcher
    {
        return $this->instantiate();
    }
}
