<?php

namespace Thessia\Twig;

use Thessia\Config\TwigConfig;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Twig
{
    protected Environment $twig;
    public function __construct(
        protected TwigConfig $twigConfig
    ) {
        if(!file_exists($this->twigConfig->cachePath())) {
            mkdir($this->twigConfig->cachePath(), 0777, true);
        }

        $loader = new FilesystemLoader($this->twigConfig->templatePath());
        $this->twig = new Environment($loader, [
            'cache' => $this->twigConfig->cachePath(),
            'debug' => $this->twigConfig->debug(),
            'auto_reload' => $this->twigConfig->autoReload(),
            'strict_variables' => $this->twigConfig->strictVariables(),
            'optimizations' => $this->twigConfig->optimizations()
        ]);
    }

    public function render(string $templatePath, array $data = []): string
    {
        if (pathinfo($templatePath, PATHINFO_EXTENSION) !== 'twig') {
            throw new \RuntimeException('Error, twig templates need to end in .twig');
        }

        return $this->twig->render($templatePath, $data);
    }

}