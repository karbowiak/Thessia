<?php

namespace Thessia\Seed;

use League\Container\Container;
use Thessia\Database\MongoCollectionInterface;

abstract class AbstractSeed
{
    protected string $collectionClass;
    


    public function getCollection(): MongoCollectionInterface
    {
        return $this->container->get($this->collectionClass);
    }

    abstract public function seed(): void;
}
