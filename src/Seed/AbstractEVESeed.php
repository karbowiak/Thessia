<?php

namespace Thessia\Seed;

use Symfony\Component\Yaml\Yaml;

use function Safe\yaml_parse;

abstract class AbstractEVESeed extends AbstractSeed
{
    protected string $sdeFileName = '';
    protected string $sdeUrl = 'https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/sde.zip';
    protected string $sdeSqlUrl = 'https://www.fuzzwork.co.uk/dump/sqlite-latest.sqlite.bz2';

    public function getYamlData(string $path = '', bool $useSymfony = true): array
    {
        $data = file_get_contents(dirname(__DIR__, 2) . '/resources/cache/sde/fsd/' .
            !empty($path) ? $path : $this->sdeFileName . '.yaml');
        if ($useSymfony) {
            return Yaml::parse($data);
        }

        return yaml_parse($data);
    }
}
