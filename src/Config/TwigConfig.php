<?php

namespace Thessia\Config;

use JetBrains\PhpStorm\Pure;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Support\Collection;

class TwigConfig
{
    private Collection $config;

    public function __construct()
    {
        $this->config = collect(require(__DIR__ . '/../../resources/config/twig.php'));
    }

    final public function templatePath(): string
    {
        return $this->config->get('template_path');
    }

    final public function cachePath(): string
    {
        return $this->config->get('cache_path');
    }

    final public function debug(): bool
    {
        return $this->config->get('debug');
    }

    final public function autoReload(): bool
    {
        return $this->config->get('auto_reload');
    }

    final public function strictVariables(): bool
    {
        return $this->config->get('strict_variables');
    }

    final public function optimizations(): bool
    {
        return $this->config->get('optimizations');
    }
}
