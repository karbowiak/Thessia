<?php

namespace Thessia\Config;

use JetBrains\PhpStorm\Pure;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Support\Collection;

class RedisConfig
{
    private Collection $config;

    public function __construct()
    {
        $this->config = collect(require(__DIR__ . '/../../resources/config/redis.php'));
    }

    final public function host(): array
    {
        return $this->config->get('host');
    }

    final public function parameters(): array
    {
        return $this->config->get('parameters');
    }
}
