<?php

namespace Thessia\Config;

use JetBrains\PhpStorm\Pure;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Support\Collection;

class MongoConfig
{
    private Collection $config;

    public function __construct()
    {
        $this->config = collect(require(__DIR__ . '/../../resources/config/mongodb.php'));
    }

    final public function servers(): array
    {
        return $this->config->get('servers');
    }

    final public function db(): string
    {
        return $this->config->get('db');
    }

    final public function options(): array
    {
        return $this->config->get('options');
    }

    final public function typeMap(): array
    {
        return $this->config->get('typeMap');
    }
}
