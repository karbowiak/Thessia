<?php

namespace Thessia\Config;

use Illuminate\Support\Collection;

class SystemConfig
{
    private Collection $config;

    public function __construct()
    {
        $this->config = collect(require(__DIR__ . '/../../resources/config/system.php'));
    }

    final public function papertrail(): array
    {
        return $this->config->get('papertrail');
    }
}
