<?php

namespace Thessia\Logger;

use Monolog\Handler\SyslogUdpHandler;
use Monolog\Logger as MonologLogger;
use Psr\Log\LoggerInterface;
use Thessia\Config\SystemConfig;

class Logger implements LoggerInterface
{
    protected \Monolog\Logger $logger;

    public function __construct()
    {
        $systemConfig = new SystemConfig();
        $this->logger = new MonologLogger('Thessia');
        $paperTrailConfig = $systemConfig->papertrail();
        $handler = new SyslogUdpHandler($paperTrailConfig['host'], $paperTrailConfig['port']);
        $this->logger->pushHandler($handler);
    }
    /**
     * @return null
     */
    public function emergency($message, array $context = array())
    {
        return $this->logger->emergency($message, $context);
    }

    /**
     * @return null
     */
    public function alert($message, array $context = array())
    {
        return $this->logger->alert($message, $context);
    }

    /**
     * @return null
     */
    public function critical($message, array $context = array())
    {
        return $this->logger->critical($message, $context);
    }

    /**
     * @return null
     */
    public function error($message, array $context = array())
    {
        return $this->logger->error($message, $context);
    }

    /**
     * @return null
     */
    public function warning($message, array $context = array())
    {
        return $this->logger->warning($message, $context);
    }

    /**
     * @return null
     */
    public function notice($message, array $context = array())
    {
        return $this->logger->notice($message, $context);
    }

    /**
     * @return null
     */
    public function info($message, array $context = array())
    {
        return $this->logger->info($message, $context);
    }

    /**
     * @return null
     */
    public function debug($message, array $context = array())
    {
        return $this->logger->debug($message, $context);
    }

    /**
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        return $this->logger->log($level, $message, $context);
    }
}
