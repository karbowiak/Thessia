<?php

namespace Thessia\Cron;

use League\Container\Container;
use Illuminate\Support\Collection;
use Kcs\ClassFinder\Finder\ComposerFinder;

class CronManager implements CronManagerInterface
{
    public function __construct(
        protected Container $container,
        protected Collection $cronjobs,
    ) {
        $finder = new ComposerFinder();
        $finder->inNamespace('Thessia\\Cronjobs');

        foreach ($finder as $className => $reflector) {
            $this->cronjobs->add($this->container->get($className));
        }
    }

    public function getCronjobs(): Collection
    {
        return $this->cronjobs;
    }
}
