<?php

namespace Thessia\Cron;

use Cron\CronExpression;

abstract class CronAbstract
{
    public string $cronTime = '* * * * *';
    public bool $running = false;

    final public function getDueTime(): \DateTime
    {
        $cron = new CronExpression($this->cronTime);
        try {
            return $cron->getNextRunDate();
        } catch (\Exception $e) {
            return new \DateTime('now');
        }
    }

    final public function isDue(): bool
    {
        $cron = new CronExpression($this->cronTime);
        return $cron->isDue();
    }

    final public function print(string $message): void
    {
        $date = date('Y-m-d H:i:s');
        echo $date . ' | ' . $message;
    }

    abstract public function handle(): void;
}
