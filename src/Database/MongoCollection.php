<?php

namespace Thessia\Database;

use Exception;
use MongoDB\Client;
use RuntimeException;
use MongoDB\UpdateResult;
use MongoDB\DeleteResult;
use MongoDB\GridFS\Bucket;
use MongoDB\InsertOneResult;
use MongoDB\BSON\UTCDateTime;
use Thessia\Redis\RedisCache;
use Illuminate\Support\Collection;
use IteratorIterator;
use Thessia\Stream\JsonCollectionStreamWriter;
use Traversable;

class MongoCollection implements MongoCollectionInterface
{
    /** @var string Name of collection in database */
    public string $collectionName = '';
    /** @var string Name of database that the collection is stored in */
    public string $databaseName = 'app';
    /** @var \MongoDB\Collection MongoDB Collection */
    public \MongoDB\Collection $collection;
    /** @var Bucket MongoDB GridFS Bucket for storing files */
    public Bucket $bucket;
    /** @var string Primary index key */
    public string $indexField = '';
    /** @var string[] $hiddenFields Fields to hide from output (ie. Password hash, email etc.) */
    public array $hiddenFields = [];
    /** @var string[] $required Fields required to insert data to model (ie. email, password hash, etc.) */
    public array $required = [];
    /** @var Collection Data collection when storing data */
    protected Collection $data;
    /** @var \MongoDB\Client MongoDB client connection */
    private Client $client;

    public function __construct(
        protected RedisCache $redis,
        MongoConnection $mongoConnection
    ) {
        $this->client = $mongoConnection->connect();
        $this->collection = $this->client
            ->selectDatabase($this->databaseName)
            ->selectCollection(!empty($this->collectionName) ? $this->collectionName : 'default');
        $this->bucket = $this->client
            ->selectDatabase($this->databaseName)
            ->selectGridFSBucket();

        $this->data = new Collection();
    }

    /**
     * @return Collection
     */
    public function find(
        array $filter = [],
        array $options = [],
        bool $showHidden = false,
        int $cacheTime = 0
    ): Collection {
        try {
            $md5 = md5(json_encode($filter, JSON_THROW_ON_ERROR));
            if ($cacheTime > 0 && $this->redis->has($md5)) {
                return collect(
                    json_decode(
                        $this->redis->get($md5),
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    )
                );
            }

            $result = $this->collection->find($filter, $options)->toArray();
            if ($cacheTime > 0) {
                $this->redis->set($md5, json_encode($result, JSON_THROW_ON_ERROR), $cacheTime);
            }

            if ($showHidden) {
                return collect($result);
            }

            return (collect($result))->forget($this->hiddenFields);
        } catch (Exception $e) {
            throw new RuntimeException('Error running query: ' . $e->getMessage());
        }
    }

    public function findOne(
        array $filter = [],
        array $options = [],
        bool $showHidden = false,
        int $cacheTime = 0
    ): Collection {
        try {
            $md5 = md5(json_encode($filter, JSON_THROW_ON_ERROR));
            if ($cacheTime > 0 && $this->redis->has($md5)) {
                return collect(
                    json_decode(
                        $this->redis->get($md5),
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    )
                );
            }

            $result = $this->find($filter, $options, $showHidden)->first();
            if ($cacheTime > 0) {
                $this->redis->set($md5, json_encode($result, JSON_THROW_ON_ERROR), $cacheTime);
            }

            return collect($result);
        } catch (Exception $e) {
            throw new RuntimeException('Error running query: ' . $e->getMessage());
        }
    }

    public function aggregate(
        array $pipeline = [],
        array $options = []
    ): Traversable {
        return $this->collection->aggregate($pipeline, $options);
    }

    public function delete(array $filter = []): DeleteResult
    {
        if (empty($filter)) {
            throw new RuntimeException('Error, filter cannot be empty when deleting');
        }

        return $this->collection->deleteOne($filter);
    }

    public function update(array $filter = [], array $update = []): UpdateResult
    {
        if (empty($filter)) {
            throw new RuntimeException('Error, filter cannot be empty when updating');
        }
        return $this->collection->updateOne($filter, $update);
    }

    final public function truncate(): void
    {
        try {
            $this->collection->drop();
        } catch (Exception $e) {
            throw new RuntimeException('Error truncating collection: ' . $e->getMessage());
        }
    }

    final public function setData(array $data = [], bool $clear = false): void
    {
        if ($clear === true) {
            $this->data = collect();
        }
        foreach ($data as $key => $value) {
            if ($this->data->has($key)) {
                $this->data->replace([$key => $value]);
            } else {
                $this->data->put($key, $value);
            }
        }
    }

    final public function getData(): Collection
    {
        return $this->data;
    }

    public function saveMany(): void
    {
        $this->collection->insertMany($this->data->all());
    }

    public function save(): UpdateResult|InsertOneResult
    {
        $this->hasRequired();
        try {
            $session = $this->client->startSession();
            $session->startTransaction([]);
            try {
                $result = $this->collection->insertOne($this->data->toArray(), ['session' => $session]);
                $session->commitTransaction();

                return $result;
            } catch (Exception $e) {
                $session->abortTransaction();
                throw new RuntimeException('Error occurred during transaction: ' . $e->getMessage());
            }
        } catch (Exception $e) {
            try {
                $session = $this->client->startSession();
                $session->startTransaction([]);

                try {
                    $result = $this->collection->replaceOne(
                        [
                            $this->indexField => $this->data->get($this->indexField)
                        ],
                        $this->data->toArray(),
                        [
                            'upsert' => true,
                            'session' => $session
                        ]
                    );
                    $session->commitTransaction();

                    return $result;
                } catch (Exception $e) {
                    $session->abortTransaction();
                    throw new RuntimeException('Error occurred during transaction: ' . $e->getMessage());
                }
            } catch (Exception $ex) {
                throw new RuntimeException("Error saving data: {$ex->getMessage()} / {$e->getMessage()}");
            }
        }
    }

    /**
     * @return self
     */
    final public function clear(array $data = []): MongoCollectionInterface
    {
        $this->data = collect(!empty($data) ? $data : null);
        return $this;
    }

    final public function count(
        array $filter = [],
        array $options = []
    ): int {
        return $this->collection->countDocuments($filter, $options);
    }

    final public function makeTimeFromDateTime(string $dateTime): UTCDateTime
    {
        $unixTime = strtotime($dateTime);
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    final public function makeTimeFromUnixTime(int $unixTime): UTCDateTime
    {
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    final public function makeTime(string|int $time): UTCDateTime
    {
        if (is_int($time)) {
            return $this->makeTimeFromUnixTime($time);
        }
        return $this->makeTimeFromDateTime($time);
    }

    /**
     * @return true
     */
    final public function hasRequired(Collection $data = null): bool
    {
        if (!empty($this->required)) {
            foreach ($this->required as $key) {
                if ($data !== null && !$data->has($key)) {
                    throw new RuntimeException('Error: ' . $key . ' does not exist in data..');
                }
                if (!$this->data->has($key)) {
                    throw new RuntimeException('Error: ' . $key . ' does not exist in data..');
                }
            }
        }

        return true;
    }

    public function createIndex(): MongoIndex
    {
        return new MongoIndex();
    }

    /**
     * @return array[]
     *
     * @psalm-return array{0: array}
     */
    public function getIndex(): array
    {
        if (empty($this->indexField)) {
            throw new RuntimeException('Error, indexField is not set - cannot create index for it.');
        }

        return [
            $this->createIndex()->addKey($this->indexField)->isUnique()->end()
        ];
    }

    public function makeDump(array $filter = [], string $filePath = null): void
    {
        if ($filePath === null || empty($filePath)) {
            throw new \RuntimeException('Error, filePath cannot be empty');
        }

        /** @var \MongoDB\Driver\Cursor|\Traversable $result */
        $result = $this->collection->find($filter);
        $iterator = new IteratorIterator($result);
        $iterator->rewind(); // Rewing to the start!
        $writer = new JsonCollectionStreamWriter($filePath);

        while ($elem = $iterator->current()) {
            $writer->push($elem);
            $iterator->next();
        }

        $writer->close();
    }
}
