<?php

namespace Thessia\Database;

use MongoDB\Client;
use Thessia\Config\MongoConfig;

class MongoConnection
{
    public function __construct(
        protected MongoConfig $mongoConfig
    )
    {
    }

    public function getConnectionString(): string
    {
        $dbServers = $this->mongoConfig->servers();
        $connectString = 'mongodb://';
        foreach ($dbServers as $server) {
            $connectString .= $server . ',';
        }
        $connectString = rtrim($connectString, ',');
        $connectString .= '/' . $this->mongoConfig->db();

        return $connectString;
    }

    public function connect(): Client
    {
        $dbOptions = $this->mongoConfig->options();
        return new Client(
            $this->getConnectionString(),
            $dbOptions,
            [
                'typeMap' => $this->mongoConfig->typeMap()
            ]
        );
    }
}
