<?php

namespace Thessia\Database;

class MongoIndex
{
    protected array $index = [];

    public function addKey(string $key, int $order = 1): MongoIndex
    {
        $this->index['key'][$key] = $order;
        return $this;
    }

    public function isUnique(): MongoIndex
    {
        $this->index['unique'] = true;
        return $this;
    }

    public function isSparse(): MongoIndex
    {
        $this->index['sparse'] = true;
        return $this;
    }
    
    public function expire(int $seconds = 3600): MongoIndex
    {
        $this->index['expireAfterSeconds'] = $seconds;
        return $this;
    }

    public function name(string $name): MongoIndex
    {
        $this->index['name'] = $name;
        return $this;
    }

    public function partialFilterExpression(array $filter): MongoIndex
    {
        $this->index['partialFilterExpression'] = $filter;
        return $this;
    }

    public function collation(array $collation): MongoIndex
    {
        $this->index['collation'] = $collation;
        return $this;
    }

    public function end(): array
    {
        return $this->index;
    }

    public function __toArray()
    {
        return $this->index;
    }
}