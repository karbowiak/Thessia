![logo](https://git.karbowiak.dk/karbowiak/Thessia/raw/branch/master/avatar.png "Thessia logo")

# Thessia

_[Embrace Eternity...] (https://www.youtube.com/watch?v=vMEWIl_WwVA)_

Thessia is a killboard for EVE-Online.

It is the 2nd iteration of Eon

# Requirements
- PHP >=8.0
- Redis
- MongoDB
- RoadRunner

# Installation
1. `composer install -o`
2. `./vendor/bin/rr get-binary`
3. `chmod +x ./rr`

# Run local server
1. `./rr serve -d`

# Internal Ports / Links
MongoDB:
- TCP: 27017

Redis:
- TCP: 6379

RoadRunner (HTTP):
- HTTP: 8080

# RabbitMQ via Helm
For reasons unknown, the password field isn't used, so to get the password to get into the maintenance panel:
```
username: user
password: echo $(kubectl get secret --namespace default rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)
```

# License
MIT Licensed
